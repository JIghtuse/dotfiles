dotfiles
========

~/.\*

Installation
------------

    git clone https://github.com/JIghtuse/dotfiles.git ~/.dotfiles
    pushd ~/.dotfiles
        stow shell
        stow bash
        stow kde
    popd
